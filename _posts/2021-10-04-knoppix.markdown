---
title:  "Creare una liveUSB knoppix"
date:   2021-10-04 15:00:00 +0200
layout: single

tagline: " "

categories: lezione
tags : 4A
---

* 1) Scaricare il file "iso" dal sito [http://www.knoppix.org/](http://www.knoppix.org/)

* 2.a) Da Linux :

	anche se credo che pochi abbiano un sistema linux già a disposizione ...
 
dal terminale lanciare il comando :

`dd status=progress if=percorso del file ISO of=/dev/device della pennetta`

a seguire il comando 

`sync`

* 2.b) Da Windows :

	microsoft non mette, per quanto ne sappia,a disposizione l'equivalente del comando dd ..
	utilizzare il programma opensource  [Win32 Disk Imager](https://win32diskimager.org/)
	
	Il programma è semplice e intuitivo .. selezionare il file iso la pennetta e cliccare su scrivi..
	
* 2.c) Da Knoppix :

	far partire il pc con knoppix inseriendo l'opzione ```knoppix noimage lang=it``` in questo modo knoppix partirà senza considerare la persistenza e non copiera i dati salvati sulla nuova pennetta
	
	dal menù Knoppix scegliere il programma "install KNOPPIX to flash disk", seguire le istruzioni..
	


