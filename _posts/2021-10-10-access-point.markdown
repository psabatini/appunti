---
title:  "Access Point "
date:   2021-10-10 08:00:00 +0200
layout: single


categories: personale 

---

## problema : fornire un accesso wi-fi ad una lan parzialmente gestita in proprio


Nel laboratorio è presente uno switch di nostra gestione, ma non abbiamo a disposizione un accesso wifi per i portatili BYOD (ne i cavetti ethernet a quanto pare :-) )

Ma abbiamo un' apparecchio denominato access point che è una macchina che ha una porta wifi e una porta ethernet.

In quanti modi possiamo collegarlo e configuralo per fornire un accesso wifi?


W Linux e W Orangepi R1

Cos'è OpiR1 ? una board pc con una porta wifi e (due) porta ethernet, 

A parte il ...

TO BE Continued ...

# BRIDGE: 

- disabilitiamo networkmanager ..

```
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service
```

- ```nano /etc/network/interfaces```

```
auto lo
iface lo inet loopback

auto br0
iface br0 inet dhcp
        bridge_ports eth0
        bridge_stp off
```        
        
- hostapd

```nano etc/default/hostapd``` 

```        
DAEMON_CONF="/etc/hostapd.conf"
```

```nano /etc/hostapd.conf```

```
ssid=SIS_BRIDGE
interface=wlan0
hw_mode=g
channel=5
bridge=br0
driver=nl80211
logger_syslog=0

logger_syslog_level=0
wmm_enabled=1
wpa=2
preamble=1
wpa_psk=844418fea2e83eea256a88c751f2823c2d5e6d52193b27cfa34234e0d6de0ab9
wpa_passphrase=koalakoa
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```




# ACCESS POINT (ROUTER + NAT )

- disabilitiamo networkmanager ..

```
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service
```

- ```nano /etc/network/interfaces```

```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

auto wlan0
allow-hotplug wlan0
iface wlan0 inet static
	hostapd /etc/hostapd.conf
	address 10.0.0.254
	netmask 255.255.255.0
```        
        
- routing + nat

ip forwarding

```sysctl -w net.ipv4.ip_forward=1 ``` 

```nano /etc/sysctl.conf```
```
# Uncomment the next line to enable packet forwarding for IPv4
net.ipv4.ip_forward=1
```



NAT

``` 
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

apt-get install iptables-persistent


``` 




- hostapd

```nano /etc/default/hostapd``` 

```        
DAEMON_CONF="/etc/hostapd.conf"
```

```nano /etc/hostapd.conf```

```
ssid=SIS_ROU
interface=wlan0
hw_mode=g
channel=8

driver=nl80211
logger_syslog=0

logger_syslog_level=0
wmm_enabled=1
wpa=2
preamble=1
wpa_psk=844418fea2e83eea256a88c751f2823c2d5e6d52193b27cfa34234e0d6de0ab9
wpa_passphrase=koalakoa
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```

```systemctl restart  hostapd.service```

-dnsmasq 

```nano /etc/dnsmasq.conf```
```
# Repeat the line for more than one interface.
interface=wlan0
# Or you can specify which interface _not_ to listen on
#except-interface=
#
.......

# Uncomment this to enable the integrated DHCP server, you need
# to supply the range of addresses available for lease and optionally
# a lease time. If you have more than one network, you will need to
# repeat this for each network on which you want to supply DHCP
# service.
dhcp-range=10.0.0.100,10.0.0.200,12h





```

to be continued .....
